-- Create database for MLB data

CREATE DATABASE mlb;

-- Create Rowstore table for battings

CREATE TABLE atbats_r (
    ab_id int,
    batter_id int,
    event text,
    g_id int,
    inning int,
    o int,
    p_score int,
    p_throws varchar(1),
    pitcher_id int,
    stand varchar(1),
    top varchar(5),
    shard key (ab_id)
);

-- Load data into Rowstore table

LOAD DATA INFILE '/tmp/data/atbats.csv.gz'
INTO TABLE atbats_r
FIELDS TERMINATED BY ','
IGNORE 1 LINES;

-- Create Columnstore table for battings

CREATE TABLE atbats_c (
    ab_id int,
    batter_id int,
    event text,
    g_id int,
    inning int,
    o int,
    p_score int,
    p_throws varchar(1),
    pitcher_id int,
    stand varchar(1),
    top varchar(5),
    shard key (ab_id),
    KEY (ab_id) USING CLUSTERED COLUMNSTORE
);

-- Load Data into Columnstore table

LOAD DATA INFILE '/tmp/data/atbats.csv.gz'
INTO TABLE atbats_c
FIELDS TERMINATED BY ','
IGNORE 1 LINES;

-- Check table statistics

SELECT * from information_schema.table_statistics;

-- Check data skew

SELECT
    DATABASE_NAME,
    TABLE_NAME,
    min(rows) as min_rows,
    max(rows) as max_rows,
    FLOOR(AVG(ROWS)) AS avg_rows,
    ROUND(STDDEV(ROWS)/AVG(ROWS),3) * 100 AS row_skew,
    min(memory_use/1024/1024) as min_memory_mb,
    max(memory_use/1024/1024) as max_memory_mb,
    FLOOR(AVG(MEMORY_USE/1024/1024)) AS avg_memory_mb,
    ROUND(STDDEV(MEMORY_USE)/AVG(MEMORY_USE),3) * 100 AS memory_skew
FROM INFORMATION_SCHEMA.TABLE_STATISTICS
GROUP BY 1, 2
ORDER BY database_name,row_skew DESC;

-- Create Sharded table for players

CREATE TABLE player_name_s (
    id int,
    first_name varchar(50),
    last_name varchar(50),
    SHARD KEY (id)
);

-- Load player data

LOAD DATA INFILE '/tmp/data/player_names.csv.gz'
INTO TABLE player_name_s
FIELDS TERMINATED BY ','
IGNORE 1 LINES;

-- Check the Left handed players

SELECT distinct first_name, last_name
FROM player_name_s
INNER JOIN atbats_r ON atbats_r.pitcher_id = player_name_s.id
WHERE atbats_r.p_throws='L';

-- Create Reference table for players

CREATE REFERENCE TABLE player_name_r (
    id int,
    first_name varchar(50),
    last_name varchar(50),
    PRIMARY KEY (id)
);

-- Load player data

LOAD DATA INFILE '/tmp/data/player_names.csv.gz'
INTO TABLE player_names_r
FIELDS TERMINATED BY ','
IGNORE 1 LINES;

-- Check the left handed players

SELECT distinct first_name, last_name
FROM player_name
INNER JOIN atbats_r ON atbats_r.pitcher_id = player_name.id
WHERE atbats_r.p_throws='L';

-- Compare with columnstore performance

SELECT distinct first_name, last_name
FROM player_name
INNER JOIN atbats_c ON atbats_c.pitcher_id = player_name.id
WHERE atbats_c.p_throws='L';


-- explain + profile

-- Create optimized batting table

CREATE TABLE atbats_c_opt (
    ab_id int,
    batter_id int,
    event text,
    g_id int,
    inning int,
    o int,
    p_score int,
    p_throws varchar(1),
    pitcher_id int,
    stand varchar(1),
    top varchar(5),
    shard key (ab_id),
    KEY (p_throws) USING CLUSTERED COLUMNSTORE with(columnstore_segment_rows=10240)
);

-- Populate the table

LOAD DATA INFILE '/tmp/data/atbats.csv.gz'
INTO TABLE atbats_c_opt
FIELDS TERMINATED BY ','
IGNORE 1 LINES;

-- Compare profiles, which is better?

profile
SELECT distinct first_name, last_name
FROM player_name
INNER JOIN atbats_c ON atbats_c.pitcher_id = player_name.id
WHERE atbats_c.p_throws='L';

profile
SELECT distinct first_name, last_name
FROM player_name
INNER JOIN atbats_c_opt ON atbats_c_opt.pitcher_id = player_name.id
WHERE atbats_c_opt.p_throws='L';


-- Create Columnstore table for the pitches

CREATE TABLE pitches (
    px decimal(8,2),
    pz decimal(8,2),
    start_speed decimal(8,2),
    end_speed decimal(8,2),
    spin_rate decimal(8,2),
    spin_dir decimal(8,2),
    break_angle decimal(8,2),
    break_length decimal(8,2),
    break_y decimal(8,2),
    ax decimal(8,2),
    ay decimal(8,2),
    az decimal(8,2),
    sz_bot decimal(8,2),
    sz_top decimal(8,2),
    type_confidence decimal(8,2),
    vx0 decimal(8,2),
    vy0 decimal(8,2),
    vz0 decimal(8,2),
    x decimal(8,2),
    x0 decimal(8,2),
    y decimal(8,2),
    y0 decimal(8,2),
    z0 decimal(8,2),
    pfx_x decimal(8,2),
    pfx_z decimal(8,2),
    nasty int,
    zone int,
    code varchar(10),
    type varchar(5),
    pitch_type varchar(5),
    event_num int,
    b_score decimal(8,2),
    ab_id int,
    b_count int,
    s_count int,
    outs int,
    pitch_num int,
    on_1b int,
    on_2b int,
    on_3b int,
    KEY (ab_id) USING CLUSTERED COLUMNSTORE
);

-- Load data into the pitches table

LOAD DATA INFILE '/tmp/data/pitches.csv.gz'
INTO TABLE pitches
FIELDS TERMINATED BY ','
IGNORE 1 LINES;

-- Select some interesting pitching info

SELECT start_speed, spin_rate, px, pz, spin_dir, first_name, last_name
FROM pitches
INNER JOIN atbats_r ON atbats_r.ab_id = pitches.ab_id
INNER JOIN player_name ON atbats_r.pitcher_id = player_name.id
ORDER BY start_speed DESC
LIMIT 10;

-- check the execution plan


-- Optimize the pitches table

CREATE TABLE pitches (
    px decimal(8,2),
    pz decimal(8,2),
    start_speed decimal(8,2),
    end_speed decimal(8,2),
    spin_rate decimal(8,2),
    spin_dir decimal(8,2),
    break_angle decimal(8,2),
    break_length decimal(8,2),
    break_y decimal(8,2),
    ax decimal(8,2),
    ay decimal(8,2),
    az decimal(8,2),
    sz_bot decimal(8,2),
    sz_top decimal(8,2),
    type_confidence decimal(8,2),
    vx0 decimal(8,2),
    vy0 decimal(8,2),
    vz0 decimal(8,2),
    x decimal(8,2),
    x0 decimal(8,2),
    y decimal(8,2),
    y0 decimal(8,2),
    z0 decimal(8,2),
    pfx_x decimal(8,2),
    pfx_z decimal(8,2),
    nasty int,
    zone int,
    code varchar(10),
    type varchar(5),
    pitch_type varchar(5),
    event_num int,
    b_score decimal(8,2),
    ab_id int,
    b_count int,
    s_count int,
    outs int,
    pitch_num int,
    on_1b int,
    on_2b int,
    on_3b int,
    KEY (ab_id) USING CLUSTERED COLUMNSTORE,
    SHARD KEY (ab_id)
);

-- Check again the execution plan